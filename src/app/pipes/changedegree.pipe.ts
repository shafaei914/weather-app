import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'changedegree'
})
export class ChangedegreePipe implements PipeTransform {

 public transform(value:number, args?:number[]): number {
    return Math.round(value-273.15);
  }

}