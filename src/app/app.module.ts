import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AddCityComponent } from './components/add-city/add-city.component';
import { CityDetailComponent } from './components/city-detail/city-detail.component';
import { CityWeatherComponent } from './components/city-weather/city-weather.component';
import { AddCardComponent } from './components/add-card/add-card.component';
import { HomePageComponent } from './components/home-page/home-page.component';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ChangedegreePipe } from './pipes/changedegree.pipe';




@NgModule({
  declarations: [
    AppComponent,
    AddCityComponent,
    CityDetailComponent,
    CityWeatherComponent,
    ChangedegreePipe,
    AddCardComponent,
    HomePageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
