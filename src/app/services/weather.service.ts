import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({

  providedIn: 'root'
  
})

export class WeatherService {

  constructor(public http: HttpClient) { }

  getCityWeather(city: string): Observable<any> {

    const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=34750b894a9f87be7fb9562192d67b54`

    return this.http.get(url);
  }

  getCityDetails(city: string): Observable<any> {
    const url = `http://api.openweathermap.org/data/2.5/forecast/?q=${city}&cnt=7&units=metric&APPID=34750b894a9f87be7fb9562192d67b54`

    return this.http.get(url);
  }

}