import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CityDetailComponent } from './components/city-detail/city-detail.component';
import { AddCityComponent } from './components/add-city/add-city.component';
import { HomePageComponent } from './components/home-page/home-page.component';

const routes: Routes = [
  {path: '', component:HomePageComponent},
  {path: 'details/:name', component:CityDetailComponent},
  {path: 'add', component:AddCityComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
