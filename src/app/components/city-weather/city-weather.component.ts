import { Component, OnInit, Input } from '@angular/core';
import { WeatherService } from '../../services/weather.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.scss']
})
export class CityWeatherComponent implements OnInit {

  @Input('city') city: string;
  @Input () addMode:boolean;
  
  constructor(private WeatherService: WeatherService, private router: Router) { }

  ngOnInit(): void {
    this.getWeatherInfo(this.city);
  }

  weatherdata: any;

  getWeatherInfo(CityName: string) {
    this.WeatherService.getCityWeather(CityName).subscribe(data => {
      this.weatherdata = {
        condition: data.weather[0].main,
        city: data.name,
        temp: data.main.temp,
        minTemp: data.main.temp_min,
        maxTemp: data.main.temp_max,
      }
      console.log(this.weatherdata);
  })

}

openDetails() {
  this.router.navigate(['details' , this.weatherdata.city]);
}

clickToAdd(cityName){
  let cityList = [];
  if(localStorage.getItem('cityList')) {
    cityList = JSON.parse(localStorage.getItem('cityList'));
  }
  cityList.push(cityName);
  localStorage.setItem('cityList', JSON.stringify(cityList));

  this.router.navigate(['/']);
}
}