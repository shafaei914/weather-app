import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { WeatherService } from "../../services/weather.service";
import { City } from 'src/model/model';

@Component({
  selector: 'app-city-detail',
  templateUrl: './city-detail.component.html',
  styleUrls: ['./city-detail.component.scss']
})

export class CityDetailComponent implements OnInit {

  constructor(public activeRouter: ActivatedRoute, public Weather: WeatherService) { }

  ngOnInit(): void {
    this.activeRouter.params.subscribe(
      params => {
        this.weatherDatails(params.name);
      }
    )
  }

  days = ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
  todayNumber = new Date().getDay();
  today = this.days[this.todayNumber];
  everyDayCondition: string[] = [];
  everyDayTemp: number[] = [];
  weatherInfo: any;
  cityName: string;
  fiveDaysData: any;
  weatherId: string;
  condition: any;

  weatherDatails(cityName: string) {
    this.Weather.getCityWeather(cityName).subscribe(data => {
      this.weatherInfo = {
        condition: data.weather[0].main,
        city: data.name,
        temp: data.main.temp,
        humidity: data.main.humidity,
        wind: data.wind.speed,
      }
      this.condition = data.weather[0].main;
      this.weatherId = data.id;
      this.fiveDaysInfo(this.weatherInfo.city);

    })
  }

  fiveDaysInfo(id: string) {
    this.Weather.getCityDetails(id).subscribe(data => {
      this.fiveDaysData = {
        list: data.list
      }

      for (let i = this.todayNumber; i < 7; i++) {
        this.everyDayTemp[i] = this.fiveDaysData.list[i].main.temp;
        this.everyDayCondition[i] = this.fiveDaysData.list[i].weather[0].main;
      }
      for (let i = this.todayNumber - 1; i >= 0; i--) {
        this.everyDayTemp[i] = Math.round(this.fiveDaysData.list[i].main.temp);
        this.everyDayCondition[i] = this.fiveDaysData.list[i].weather[0].main;
      }

    })
  }

}