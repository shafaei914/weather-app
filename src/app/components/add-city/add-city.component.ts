import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../../services/weather.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.scss']
})

export class AddCityComponent implements OnInit {
  cityForm: FormGroup;
  cityInfo: string;
  city: any;

  constructor(private apiService: WeatherService, private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.cityForm = new FormGroup({
      cityName: new FormControl('')
    })
  }

  getCity(city: any) {
    this.apiService.getCityWeather(city).subscribe(data => {
      console.log(data);
      this.city = data;
    })
  }

  search() {
    this.cityInfo = this.cityForm.get('cityName').value;
    this.getCity(this.cityInfo);
  }

}